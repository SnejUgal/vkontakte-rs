# `vkontakte-rs`

The `vkontakte` crate lets you work with VK API from Rust easily.
It provides wrappers around methods with types related to these methods.
At the same time, the crate is pure and does not assume how you run your
futures.

However, `vkontakte` does have some restrictions:

- The same version of this crate supports only one API version; the current
  version is `5.92`;
- Connectors in clients you pass must support HTTPS.

## Installing

Add this to your `Cargo.toml`:

```toml
[dependencies.vkontakte]
git = "https://gitlab.com/snejugal/vkontakte-rs.git"
```

## Example: check if a link is safe

```rust
use vkontakte::methods::utils::CheckLink;
use futures::{Future, IntoFuture};

let client = get_hyper_client_somehow();
let check_link = CheckLink::new(&client, TOKEN, "https://rust-lang.org")
    .into_future()
    .map(|check_result| {
        dbg!(check_result);
    })
    .map_err(|error| {
        dbg!(error);
    });

tokio::run(check_link);
```
