const TOKEN: &str = env!("VK_TOKEN");
const URL: &str = "https://rust-lang.org";

use futures::{Future, IntoFuture};
use vkontakte::methods::utils;

fn check_link() -> impl Future<Item = (), Error = ()> {
    let cpus = num_cpus::get();
    let connector = hyper_tls::HttpsConnector::new(cpus).unwrap();

    let client = hyper::Client::builder().build(connector);

    utils::CheckLink::new(&client, TOKEN, URL)
        .https(true)
        .into_future()
        .map(|check_result| {
            dbg!(check_result);
        })
        .map_err(|error| {
            dbg!(error);
        })
}

fn main() {
    tokio::run(check_link());
}
