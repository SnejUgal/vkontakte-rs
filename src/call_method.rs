use crate::types::{self, Response};
use futures::{Future, Stream};
use hyper::{
    client::{connect::Connect, ResponseFuture},
    Body, Client, Method, Request, Uri,
};
use serde::de::DeserializeOwned;

#[must_use]
fn create_url(method: &'static str) -> Uri {
    let path = format!("/method/{}", method);

    Uri::builder()
        .scheme("https")
        .authority("api.vk.com")
        .path_and_query(path.as_str())
        .build()
        .unwrap()
}

#[must_use]
fn create_request(method: &'static str, body: String) -> Request<Body> {
    let mut request = Request::new(Body::from(body));
    *request.method_mut() = Method::POST;
    *request.uri_mut() = create_url(method);

    request
}

#[must_use]
fn process_response<T: DeserializeOwned>(
    response: ResponseFuture,
) -> impl Future<Item = T, Error = types::Error> {
    response
        .and_then(|response| response.into_body().concat2())
        .map_err(types::Error::NetworkError)
        .and_then(|response| {
            serde_json::from_slice(&response[..])
                .map_err(types::Error::InvalidResponse)
        })
        .and_then(|response| match response {
            Response::Ok(response) => Ok(response),
            Response::Error(error) => Err(types::Error::RequestError(error)),
        })
}

#[must_use]
pub fn call<'a, T, C>(
    method: &'static str,
    client: &'a Client<C>,
    body: String,
) -> impl Future<Item = T, Error = types::Error>
where
    T: DeserializeOwned,
    C: Connect + 'static,
{
    let request = create_request(method, body);
    let response = client.request(request);

    process_response(response)
}
