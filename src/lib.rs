//! Work with VK API using Rust and futures.
//!
//! The `vkontakte` crate lets you work with VK API from Rust easily.
//! It provides wrappers around methods with types related to these methods.
//! At the same time, the crate is pure and does not assume how you run your
//! futures.
//!
//! However, `vkontakte` does have some restrictions:
//!
//! - The same version of this crate supports only one API version; the current
//!   version is `5.92`;
//! - Connectors in clients you pass must support HTTPS.
//!
//! # Example: check if a link is safe
//!
//! ```no_run
//! # const TOKEN: &str = "";
//! #
//! # use hyper::client::connect::HttpConnector;
//! # fn get_hyper_client_somehow() -> hyper::Client<HttpConnector> {
//! #     hyper::Client::new()
//! # }
//! use vkontakte::methods::utils::CheckLink;
//! use futures::{Future, IntoFuture};
//!
//! let client = get_hyper_client_somehow();
//! let check_link = CheckLink::new(&client, TOKEN, "https://rust-lang.org")
//!     .into_future()
//!     .map(|check_result| {
//!         dbg!(check_result);
//!     })
//!     .map_err(|error| {
//!         dbg!(error);
//!     });
//!
//! tokio::run(check_link);
//! ```

#![feature(trait_alias)]
#![deny(future_incompatible)]
#![deny(nonstandard_style)]
#![deny(missing_docs)]

mod call_method;

#[macro_use]
pub mod methods;
pub mod types;

use call_method::call;
use url::form_urlencoded::Serializer;

#[rustfmt::skip]
#[doc(hidden)]
pub trait Connector = hyper::client::connect::Connect + Sync + 'static;

type BoxedFuture<I, E> = Box<dyn futures::Future<Item = I, Error = E> + Send>;
type Form = url::form_urlencoded::Serializer<String>;
