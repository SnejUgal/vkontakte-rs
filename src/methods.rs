//! Contains representations of VK API methods.

#[macro_use]
mod method_macros;

pub mod utils;
pub mod wall;
