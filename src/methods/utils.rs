//! Contains methods from the [`utils`] group.
//!
//! [`utils`]: https://vk.com/dev/utils

mod check_link;

pub use check_link::*;
