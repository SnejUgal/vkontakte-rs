macro_rules! doc_comment {
    ($x:expr, $($tt:tt)*) => {
        #[doc = $x]
        $($tt)*
    };
}

macro_rules! method {
    (
        struct $name:ident<'a> {
            $(req $field:ident: $type:ty,)*
            $(opt $opt_field:ident: $opt_type:ty,)*
        }

        const METHOD_NAME = $vk_name:literal;
        type ResponseType = $response_type:ty;
        let generate_body = $generate_body:expr;
    ) => {
        use crate::{
            call,
            types,
            BoxedFuture, Connector, Form, Serializer,
        };
        use futures::IntoFuture;
        doc_comment! {
            concat!(
                "Represents [`", $vk_name, "`]",
                "(https://vk.com/dev/", $vk_name, ").",
            ),
            #[derive(Clone, Copy)]
            #[must_use = "methods do nothing unless turned into future"]
            pub struct $name<'a, C> {
                client: &'a hyper::Client<C>,
                token: &'a str,
                lang: Option<&'a str>,
                https: Option<bool>,
                test_mode: Option<bool>,
                $($field: $type,)*
                $($opt_field: Option<$opt_type>,)*
            }
        }

        impl<'a, C> $name<'a, C> {
            const METHOD_NAME: &'static str = $vk_name;

            doc_comment! {
                concat!("Constructs a new `", stringify!($name), "`."),
                pub fn new(
                    client: &'a hyper::Client<C>,
                    token: &'a str,
                    $($field: $type,)*
                ) -> Self {
                    Self {
                        client,
                        token,
                        lang: None,
                        https: None,
                        test_mode: None,
                        $($field,)*
                        $($opt_field: None,)*
                    }
                }
            }

            $(
                doc_comment! {
                    concat!("Sets `", stringify!($opt_field), "`."),
                    pub fn $opt_field(
                        mut self,
                        $opt_field: $opt_type,
                    ) -> Self {
                        self.$opt_field = Some($opt_field);
                        self
                    }
                }
            )*

            /// Sets the language for displayable data.
            pub fn lang(mut self, lang: &'a str) -> Self {
                self.lang = Some(lang);
                self
            }

            /// Sets whether VK will return HTTPS links.
            pub fn https(mut self, https: bool) -> Self {
                self.https = Some(https);
                self
            }

            /// Allows to send methods witoout activating the app for all users.
            pub fn test_mode(mut self, test_mode: bool) -> Self {
                self.test_mode = Some(test_mode);
                self
            }
        }

        impl<'a, C: Connector> IntoFuture for $name<'a, C> {
            type Item = $response_type;
            type Error = types::Error;
            type Future = BoxedFuture<Self::Item, Self::Error>;

            fn into_future(self) -> Self::Future {
                let client = self.client;
                let mut form = Serializer::new(String::new());

                form.append_pair("access_token", self.token)
                    .append_pair("v", "5.92");

                if let Some(lang) = self.lang {
                    form.append_pair("lang", lang);
                }
                if let Some(https) = self.https {
                    form.append_pair("https", &(https as u8).to_string());
                }
                if let Some(test_mode) = self.test_mode {
                    form.append_pair(
                        "test_mode",
                        &(test_mode as u8).to_string()
                    );
                }

                type Generator<T> = fn(method: $name<T>, form: &mut Form);
                let generate_body: Generator<_> = $generate_body;

                generate_body(self, &mut form);

                let body = form.finish();

                let future = call(Self::METHOD_NAME, client, body);

                Box::new(future)
            }
        }
    };
}
