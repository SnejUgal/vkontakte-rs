use serde::Deserialize;

/// Represents the results of getting posts.
#[derive(Deserialize, Debug, PartialEq, Clone)]
pub struct GetResult {
    /// Total amount of posts.
    pub count: u32,
    /// Requested posts.
    pub items: Vec<types::Post>,
}

method! {
    struct Get<'a> {
        opt owner_id: i32,
        opt domain: &'a str,
        opt offset: u32,
        opt count: u8,
        opt filter: types::WallFilter,
        // opt extended: bool, // Perhaps another method for that
        opt fields: &'a [&'a str],
    }

    const METHOD_NAME = "wall.get";
    type ResponseType = GetResult;
    let generate_body = |method, form| {
        if let Some(owner_id) = method.owner_id {
            form.append_pair("owner_id", &owner_id.to_string());
        }
        if let Some(domain) = method.domain {
            form.append_pair("domain", domain);
        }
        if let Some(offset) = method.offset {
            form.append_pair("offset", &offset.to_string());
        }
        if let Some(count) = method.count {
            form.append_pair("count", &count.to_string());
        }
        if let Some(filter) = method.filter {
            form.append_pair("domain", &filter.to_string());
        }
        if let Some(fields) = method.fields {
            form.append_pair("fields", &fields.join(","));
        }
    };
}
