use serde::Deserialize;

/// Represents the status whether the link is safe.
#[derive(Deserialize, Debug, PartialEq, Eq, Hash, Clone, Copy)]
#[serde(rename_all = "snake_case")]
pub enum Status {
    /// The link is safe.
    NotBanned,
    /// The link is **not** safe.
    Banned,
    /// The link is being scanned.
    Processing,
}

/// Represents the results of checking the link.
#[derive(Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct CheckLinkResult {
    /// Status whether the link is safe.
    pub status: Status,
    /// The full link that was checked, resolving shortened links.
    pub link: String,
}

method! {
    struct CheckLink<'a> {
        req url: &'a str,
    }

    const METHOD_NAME = "utils.checkLink";
    type ResponseType = CheckLinkResult;
    let generate_body = |method, form| {
        form.append_pair("url", method.url);
    };
}
