//! Contains methods from the [`walls`] group.
//!
//! [`walls`]: https://vk.com/dev/walls

mod get;

pub use get::*;
