//! Contains types to work with VK API.

mod attachment;
mod error;
mod post;
mod wall_filter;

pub use {attachment::*, error::*, post::*, wall_filter::*};

mod response;
pub(crate) use response::*;

use serde::de::{Deserializer, Unexpected, Visitor};
use std::fmt;

struct FlagVisitor;

impl<'v> Visitor<'v> for FlagVisitor {
    type Value = bool;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "a numeric value of 0 or 1")
    }

    fn visit_u64<E: serde::de::Error>(self, v: u64) -> Result<Self::Value, E> {
        if v >= 2 {
            Err(E::invalid_value(
                Unexpected::Other("value non-castable to bool"),
                &self,
            ))
        } else {
            Ok(v == 1)
        }
    }
}

fn flag_to_bool<'de, D: Deserializer<'de>>(d: D) -> Result<bool, D::Error> {
    d.deserialize_u8(FlagVisitor)
}
