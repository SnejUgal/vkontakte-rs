use std::fmt;

/// Types of wall posts.
#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub enum WallFilter {
    /// The owner's posts.
    Owner,
    /// Others' posts.
    Others,
    /// Everyone's posts.
    All,
    /// Posts to be posted in the future.
    Postponed,
    /// Suggested posts.
    Suggests,
}

impl fmt::Display for WallFilter {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        let string = match self {
            WallFilter::Owner => "owner",
            WallFilter::Others => "others",
            WallFilter::All => "all",
            WallFilter::Postponed => "postponed",
            WallFilter::Suggests => "suggests",
        };

        write!(formatter, "{}", string)
    }
}
