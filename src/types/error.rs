use serde::{
    de::{Deserializer, SeqAccess, Visitor},
    Deserialize,
};
use std::collections::HashMap;
use std::fmt;

#[derive(Deserialize)]
struct Parameter {
    key: String,
    value: String,
}

type Parameters = HashMap<String, String>;

/// Represents an error from VK.
#[derive(Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct VkError {
    /// Error code.
    #[serde(rename = "error_code")]
    pub code: u16,
    /// Human readable error message.
    #[serde(rename = "error_msg")]
    pub message: String,
    /// Parameters that were passed to the method.
    #[serde(rename = "request_params")]
    #[serde(deserialize_with = "deserialize_parameters")]
    pub request_paramaters: Parameters,
}

/// Represents possible errors that may happen while calling a method.
#[derive(Debug)]
pub enum Error {
    /// VK response could not be parsed &mdash; this is probably a bug
    /// in the crate. In this case, feel free to fill an issue on
    /// [our GitLab repository][gitlab].
    ///
    /// [gitlab]: https://gitlab.com/SnejUgal/vkontakte-rs
    InvalidResponse(serde_json::error::Error),
    /// Network error while calling a method.
    NetworkError(hyper::Error),
    /// VK returned an error in response.
    RequestError(VkError),
}

struct ParametersVisitor;

impl<'v> Visitor<'v> for ParametersVisitor {
    type Value = Parameters;
    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "an array of objects with `key` and `value`")
    }

    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
    where
        A: SeqAccess<'v>,
    {
        let mut parameters = HashMap::new();

        while let Some(parameter) = seq.next_element::<Parameter>()? {
            parameters.insert(parameter.key, parameter.value);
        }

        Ok(parameters)
    }
}

fn deserialize_parameters<'de, D>(d: D) -> Result<Parameters, D::Error>
where
    D: Deserializer<'de>,
{
    d.deserialize_seq(ParametersVisitor)
}
