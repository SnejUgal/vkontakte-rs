use super::{flag_to_bool, Attachment};
use serde::Deserialize;

/// Represents data relating to replying.
#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub struct Reply {
    /// The original post owner's ID.
    pub owner_id: i32,
    /// The original post's ID.
    pub post_id: i32,
}

/// Represents data relating to comments.
#[derive(Deserialize, Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub struct Comments {
    /// Total amount of comments to this post.
    pub count: u32,
    /// Whether the user can post a comment.
    #[serde(deserialize_with = "flag_to_bool")]
    pub can_post: bool,
    /// Whether the user can post a comment as a group.
    pub groups_can_post: Option<bool>,
}

/// Represents data relating to likes.
#[derive(Deserialize, Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub struct Likes {
    /// Total number of likes.
    pub count: u32,
    /// Whethe the user liked this post.
    #[serde(deserialize_with = "flag_to_bool")]
    pub user_likes: bool,
    /// Whether the user can like this post.
    #[serde(deserialize_with = "flag_to_bool")]
    pub can_like: bool,
    /// Whethe the user can report this post.
    #[serde(deserialize_with = "flag_to_bool")]
    pub can_publish: bool,
}

/// Represetns data related to reposts.
#[derive(Deserialize, Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub struct Reposts {
    /// Total number of reposts.
    pub count: u32,
    /// Whether the user reposted this post.
    #[serde(deserialize_with = "flag_to_bool")]
    pub user_reposted: bool,
}

/// Represents which kind of post this one is.
#[derive(Deserialize, Debug, PartialEq, Eq, Clone, Copy, Hash)]
#[serde(rename_all = "snake_case")]
pub enum PostKind {
    /// Simple post.
    Post,
    /// Repost.
    Copy,
    /// Repost (?),
    Reply,
    /// Scheduled post.
    Postpone,
    /// Suggested post.
    Suggest,
}

/// Represents data aobut a location.
#[derive(Deserialize, Debug, PartialEq, Clone)]
pub struct Geo {
    /// The place's ID.
    pub place_id: u32,
    /// The place's title.
    pub title: String,
    /// The place's kind,
    #[serde(rename = "type")]
    pub kind: String,
    /// The place's country ID.
    pub country_id: u32,
    /// The place's city ID.
    pub city_id: u32,
    /// The place's address.
    pub address: String,
    /// The place's showmap.
    pub showmap: serde_json::Value, // who knows what the heck it is
}

/// Represents a post source.
#[derive(Deserialize, Debug, PartialEq, Eq, Clone, Copy, Hash)]
#[serde(tag = "type")]
#[serde(rename_all = "snake_case")]
pub enum PostSource {
    /// The post is caused by an action on VK.
    Vk,
    /// The post is caused by a widget.
    Widget,
    /// The post was creating using API.
    Api,
    /// The post was mirrored from RSS.
    Rss,
    /// The post was sent via SMS.
    Sms,
    /// The post was created with mobile VK version.
    Mvk,
}

/// Represents a post.
#[derive(Debug, PartialEq, Clone)]
pub struct Post {
    /// The post's ID.
    pub id: u32,
    /// The post owner's ID.
    pub owner_id: i32,
    /// The ID of the user who posted the post.
    pub from_id: i32,
    /// Unix time of the post.
    pub date: u64,
    /// Text of the post.
    pub text: String,
    /// If the post is a reply, contains data about the original post.
    pub reply: Option<Reply>,
    /// Data about comments to this post.
    pub comments: Option<Comments>,
    /// Data about likes of this post.
    pub likes: Option<Likes>,
    /// Data about reposts of this post.
    pub reposts: Option<Reposts>,
    /// The post's kind.
    pub kind: PostKind,
    /// Data aobut the post source.
    pub post_source: PostSource,
    /// Attachments to this post.
    pub attachments: Vec<Attachment>,
    /// Location where the post was posted at.
    pub geo: Option<Geo>,
    /// The signer's ID.
    pub signer_id: Option<u32>,
    /// History of reposts.
    pub copy_history: Vec<Post>,
    /// Whether the user can pin this post.
    pub can_pin: Option<bool>,
    /// Whether the post is pinned.
    pub is_pinned: Option<bool>,
}

const ID: &str = "id";
const OWNER_ID: &str = "owner_id";
const FROM_ID: &str = "from_id";
const DATE: &str = "date";
const TEXT: &str = "text";
const REPLY_OWNER_ID: &str = "reply_owner_id";
const REPLY_POST_ID: &str = "reply_post_id";
const COMMENTS: &str = "comments";
const LIKES: &str = "likes";
const REPOSTS: &str = "reposts";
const KIND: &str = "post_type";
const POST_SOURCE: &str = "post_source";
const ATTACHMENTS: &str = "attachments";
const GEO: &str = "geo";
const SIGNER_ID: &str = "signer_id";
const COPY_HISTORY: &str = "copy_history";
const CAN_PIN: &str = "can_pin";
const IS_PINNED: &str = "is_pinned";

use serde::de::{self, Deserializer, MapAccess, Visitor};
use std::fmt;

struct PostVisitor;

impl<'v> Visitor<'v> for PostVisitor {
    type Value = Post;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "an object of the Post type")
    }

    fn visit_map<A>(self, mut map: A) -> Result<Self::Value, A::Error>
    where
        A: MapAccess<'v>,
    {
        let mut id = None;
        let mut owner_id = None;
        let mut from_id = None;
        let mut date = None;
        let mut text = None;
        let mut reply_owner_id = None;
        let mut reply_post_id = None;
        let mut comments = None;
        let mut likes = None;
        let mut reposts = None;
        let mut kind = None;
        let mut post_source = None;
        let mut attachments = None;
        let mut geo = None;
        let mut signer_id = None;
        let mut copy_history = None;
        let mut can_pin = None;
        let mut is_pinned = None;

        while let Some(key) = map.next_key()? {
            match key {
                ID => id = Some(map.next_value()?),
                OWNER_ID => owner_id = Some(map.next_value()?),
                FROM_ID => from_id = Some(map.next_value()?),
                DATE => date = Some(map.next_value()?),
                TEXT => text = Some(map.next_value()?),
                REPLY_OWNER_ID => reply_owner_id = Some(map.next_value()?),
                REPLY_POST_ID => reply_post_id = Some(map.next_value()?),
                COMMENTS => comments = Some(map.next_value()?),
                LIKES => likes = Some(map.next_value()?),
                REPOSTS => reposts = Some(map.next_value()?),
                KIND => kind = Some(map.next_value()?),
                POST_SOURCE => post_source = Some(map.next_value()?),
                ATTACHMENTS => attachments = Some(map.next_value()?),
                GEO => geo = Some(map.next_value()?),
                SIGNER_ID => signer_id = Some(map.next_value()?),
                COPY_HISTORY => copy_history = Some(map.next_value()?),
                CAN_PIN => can_pin = Some(map.next_value::<u8>()? == 1),
                IS_PINNED => is_pinned = Some(map.next_value::<u8>()? == 1),
                _ => {
                    let _ = map.next_value::<serde_json::Value>()?;
                }
            }
        }

        Ok(Post {
            id: id.ok_or_else(|| de::Error::missing_field(ID))?,
            owner_id: owner_id
                .ok_or_else(|| de::Error::missing_field(OWNER_ID))?,
            from_id: from_id
                .ok_or_else(|| de::Error::missing_field(FROM_ID))?,
            date: date.ok_or_else(|| de::Error::missing_field(DATE))?,
            text: text.unwrap_or_else(String::new),
            reply: reply_owner_id.and_then(|owner_id| {
                reply_post_id.map(|post_id| Reply {
                    owner_id,
                    post_id,
                })
            }),
            comments,
            likes,
            reposts,
            kind: kind.ok_or_else(|| de::Error::missing_field(KIND))?,
            post_source: post_source
                .ok_or_else(|| de::Error::missing_field(POST_SOURCE))?,
            attachments: attachments.unwrap_or_else(Vec::new),
            geo,
            signer_id,
            copy_history: copy_history.unwrap_or_else(Vec::new),
            can_pin,
            is_pinned,
        })
    }
}

impl<'de> de::Deserialize<'de> for Post {
    fn deserialize<D: Deserializer<'de>>(d: D) -> Result<Self, D::Error> {
        d.deserialize_struct(
            "Post",
            &[
                ID,
                OWNER_ID,
                FROM_ID,
                DATE,
                TEXT,
                REPLY_OWNER_ID,
                REPLY_POST_ID,
                COMMENTS,
                LIKES,
                REPOSTS,
                KIND,
                POST_SOURCE,
                ATTACHMENTS,
                GEO,
                SIGNER_ID,
                COPY_HISTORY,
                CAN_PIN,
                IS_PINNED,
            ],
            PostVisitor,
        )
    }
}
