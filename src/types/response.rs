use super::VkError;
use serde::Deserialize;

#[derive(Deserialize, Debug, PartialEq, Eq, Clone)]
#[serde(rename_all = "snake_case")]
pub enum Response<T> {
    #[serde(rename = "response")]
    Ok(T),
    Error(VkError),
}
