use serde::Deserialize;

// Let the developers of VK API burn in hell.

/// Represents a photo, either from am album or posted directly.
#[derive(Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct Photo {
    /// The photo's ID.
    pub id: u32,
    /// The photo album's ID.
    pub album_id: i32,
    /// The photo owner's ID.
    pub owner_id: i32,
    /// The photo uploader's ID.
    pub user_id: Option<i32>,
}

/// Represents a video.
#[derive(Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct Video {
    /// The video's ID.
    id: u32,
    /// The video owner's ID.
    owner_id: i32,
    /// The video's title.
    title: String,
    /// The video's duration.
    duration: u32,
}

/// Represents an audio.
#[derive(Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct Audio {
    /// The audio's ID.
    id: u32,
    /// The audio owner's ID.
    owner_id: i32,
    /// The audio's title.
    title: String,
    /// The audio's duration.
    duration: u32,
}

/// Represents a document.
#[derive(Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct Document {
    /// The document's ID.
    id: u32,
    /// The document owner's ID.
    owner_id: i32,
    /// The document's title.
    title: String,
    /// The document's size.
    size: u32,
    /// The document's extension.
    ext: String,
}

/// Represents a graffiti.
#[derive(Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct Graffiti {
    /// The graffiti's ID.
    id: u32,
    /// The graffiti owner's ID.
    owner_id: i32,
    /// Preivew URL.
    src: String,
    /// Full-size graffity URL.
    src_big: String,
}

/// Represents a link.
#[derive(Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct Link {
    /// Link URL.
    url: String,
    /// Link title.
    title: String,
    /// Link description.
    description: String,
    /// Link preview URL.
    image_src: Option<String>,
}

/// Represents a note.
#[derive(Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct Note {
    /// The note's ID.
    id: u32,
    /// The note owner's ID.
    owner_id: i32,
    /// The note's title.
    title: String,
    /// Amount of the note's comments.
    #[serde(rename = "ncom")]
    comments_count: u32,
}

/// Represents an app.
#[derive(Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct App {
    /// The app's ID.
    id: u32,
    /// The app's name.
    #[serde(rename = "app_name")]
    name: String,
    /// Preview URL.
    src: String,
    /// Full-size photo URL.
    src_big: String,
}

/// Represents a poll.
#[derive(Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct Poll {
    /// The poll's ID.
    id: u32,
    /// The poll's question.
    question: String,
}

/// Represents a wiki page.
#[derive(Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct Page {
    /// The page's ID.
    id: u32,
    /// The ID of the group that created the page.
    group_id: i32,
    /// The title of the page.
    title: String,
}

/// Represents an attachment.
#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Attachment {
    /// A photo from an album.
    Photo(Photo),
    /// A photo uploaded directly.
    PostedPhoto(Photo),
    /// A video.
    Video(Video),
    /// An audio.
    Audio(Audio),
    /// A document.
    Document(Document),
    /// A graffiti.
    Graffiti(Graffiti),
    /// A link preview.
    Link(Link),
    /// A note.
    Note(Note),
    /// An app.
    App(App),
    /// A poll.
    Poll(Poll),
    /// A wiki page.
    Page(Page),
}

const PHOTO: &str = "photo";
const POSTED_PHOTO: &str = "posted_photo";
const VIDEO: &str = "video";
const AUDIO: &str = "audio";
const DOCUMENT: &str = "doc";
const GRAFFITI: &str = "graffiti";
const LINK: &str = "link";
const NOTE: &str = "note";
const APP: &str = "app";
const POLL: &str = "poll";
const PAGE: &str = "page";
const TYPE: &str = "type";

const FIELDS: &[&str] = &[
    PHOTO,
    POSTED_PHOTO,
    VIDEO,
    AUDIO,
    DOCUMENT,
    GRAFFITI,
    LINK,
    NOTE,
    APP,
    POLL,
    PAGE,
    TYPE,
];

use serde::de::{self, Deserializer, MapAccess, Visitor};
use std::fmt;

struct AttachmentVisitor;

impl<'v> Visitor<'v> for AttachmentVisitor {
    type Value = Attachment;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "an attachment")
    }

    fn visit_map<A: MapAccess<'v>>(
        self,
        mut map: A,
    ) -> Result<Self::Value, A::Error> {
        let mut attachment = None;

        while let Some(key) = map.next_key()? {
            match key {
                TYPE => {
                    let _ = map.next_value::<String>();
                }
                _ if attachment.is_some() => {
                    return Err(de::Error::custom("duplicate attachment"));
                }
                PHOTO => {
                    attachment = Some(Attachment::Photo(map.next_value()?))
                }
                POSTED_PHOTO => {
                    attachment =
                        Some(Attachment::PostedPhoto(map.next_value()?))
                }
                VIDEO => {
                    attachment = Some(Attachment::Video(map.next_value()?))
                }
                AUDIO => {
                    attachment = Some(Attachment::Audio(map.next_value()?))
                }
                DOCUMENT => {
                    attachment = Some(Attachment::Document(map.next_value()?))
                }
                GRAFFITI => {
                    attachment = Some(Attachment::Graffiti(map.next_value()?))
                }
                LINK => attachment = Some(Attachment::Link(map.next_value()?)),
                NOTE => attachment = Some(Attachment::Note(map.next_value()?)),
                APP => attachment = Some(Attachment::App(map.next_value()?)),
                POLL => attachment = Some(Attachment::Poll(map.next_value()?)),
                PAGE => attachment = Some(Attachment::Page(map.next_value()?)),
                _ => return Err(de::Error::unknown_field(key, FIELDS)),
            }
        }

        attachment.ok_or_else(|| de::Error::custom("no attachment"))
    }
}

impl<'de> de::Deserialize<'de> for Attachment {
    fn deserialize<D: Deserializer<'de>>(d: D) -> Result<Self, D::Error> {
        d.deserialize_struct("Attachment", FIELDS, AttachmentVisitor)
    }
}
